import {defineConfig} from 'vite'
import react from '@vitejs/plugin-react'
import svgr from 'vite-plugin-svgr'

const includePathsToBundle = [
    './src', // this should be sourceDirectory from @jsmdg/react-fragment-scripts/paths
    /node_modules\/(@jsmdg\/react-fragment-scripts\/fragment|@jsmdg\/yoshi|@jsmdg\/react-tenant-ui-library)\/.*/,
];

// https://vitejs.dev/config/
export default defineConfig({
    build: {
        outDir: 'dist',
        emptyOutDir: true,
        rollupOptions: {
            output: {
                assetFileNames: 'static/media/[name].[hash][extname]'
            }
        }
    },
    plugins: [
        react({
            include: includePathsToBundle
        }),
        svgr(
            {
                include: /\.svg$/,
                svgrOptions: {
                    ref: true,
                    titleProp: true,
                    svgoConfig: {
                        plugins: [
                            {
                                name: 'preset-default',
                                params: {
                                    overrides: {
                                        removeViewBox: false,
                                    },
                                },
                            },
                            'removeXMLNS',
                        ],
                    },
                },
            }
        ),
    ],
})

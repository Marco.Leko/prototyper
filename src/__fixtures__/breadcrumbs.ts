export const breadcrumbLevel0InTree = [
    {
        id: '26n8g',
        title: 'Kunst, Kultur & Lifestyle',
        url: '/kunst-kultur-lifestyle/l/26n8g',
    },
];

export const breadcrumbNotInTree = [
    {
        id: 'notInTreeId',
        title: 'Fake Title',
        url: '/not-in-tree/l/notInTreeId',
    },
];

export const breadcrumbLevel1InTree = [
    {
        id: '26n8g',
        title: 'Kunst, Kultur & Lifestyle',
        url: '/kunst-kultur-lifestyle/l/26n8g',
    },
    {
        id: 'cq3ye',
        title: 'Musik & Tanz',
        url: '/kunst-kultur-lifestyle/musik-tanz/l/cq3ye',
    },
];

// Page not in navigation tree
export const breadcrumbTanzkurse = [
    {
        id: '26n8g',
        title: 'Kunst, Kultur & Lifestyle',
        url: '/kunst-kultur-lifestyle/l/26n8g',
    },
    {
        id: 'cq3ye',
        title: 'Musik & Tanz',
        url: '/kunst-kultur-lifestyle/musik-tanz/l/cq3ye',
    },
    {
        id: 'ydfux',
        title: 'Tanzkurse',
        url: '/kunst-kultur-lifestyle/tanzkurse/l/ydfux',
    },
];

// Page not in navigation tree
export const breadcrumbMotorradTrainings = [
    {
        id: 'p6jmw',
        title: 'Motorpower',
        url: '/motorpower/l/p6jmw',
    },
    {
        id: 'wib1x',
        title: 'Motorräder',
        url: '/motorpower/motorraeder/l/wib1x',
    },
    {
        id: 'cq3ye',
        title: 'Motorradtrainings',
        url: '/motorpower/motorradtrainings/l/a51yr',
    },
];

export const breadcrumbGiftsForGirlfriend = [
    {
        id: 'fuer-frauen',
        title: 'Geschenke für Frauen',
        uri: '/besondere-geschenke/fuer-frauen,default,sc.html',
    },
    {
        id: 'geschenk-freundin',
        title: 'Geschenke für die Freundin',
        uri: '/geschenkidee/geschenk-freundin,default,sc.html',
    },
];

export const breadcrumbFirmenangebote = [
    {
        id: 'firmenveranstaltungen',
        title: 'Firmenveranstaltungen',
        uri: '/firmenangebote/firmenveranstaltungen,default,sc.html',
    },
];

export const breadcrumbLvl4SeoPage = [
    {
        id: '26n8g',
        title: 'Kunst, Kultur & Lifestyle',
        url: '/kunst-kultur-lifestyle/l/26n8g',
    },
    {
        id: 'cq3ye',
        title: 'Musik & Tanz',
        url: '/kunst-kultur-lifestyle/musik-tanz/l/cq3ye',
    },
    {
        id: 'odk7v',
        title: 'GOP Varieté-Theater',
        url: '/kunst-kultur-lifestyle/gop-variete-theater/l/odk7v',
    },
    {
        id: 'ypvq7',
        title: 'GOP Varieté Theater in Berlin',
        url: '/kunst-kultur-lifestyle/gop-variete-theater-berlin/l/ypvq7',
    },
];

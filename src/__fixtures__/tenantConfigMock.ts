import { TenantConfig } from '../shared/types/tenantConfig';

export const tenantConfigMock: TenantConfig = {
    giftBoxPages: [
        '/geschenkbox/',
        '/fun-action-erlebnisboxen/',
        '/kurzurlaub-erlebnisboxen/',
        '/wellness-lifestyle-erlebnisboxen/',
        '/zeit-zu-zweit-erlebnisboxen/',
    ],
    glossarPageId: 'glossar',
    allExperiencesPageId: '79ifa',
};

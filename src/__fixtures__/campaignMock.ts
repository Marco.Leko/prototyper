import { CreativePlaceholderId } from '../shared/enums';

export const campaignMock = [
    {
        campaignName: 'Black Week',
        placeholderId: CreativePlaceholderId.NavigationBannerDesktop,
        url: 'http://foo.bar',
        trackingName: 'navigation_banner_desktop_black_week',
        image: {
            url: 'https://dummyimage.com/250x120/007aa5?text=Navigation-Banner-Desktop',
            alt: 'Navigation-Banner-Desktop',
        },
    },
    {
        campaignName: 'Black Week',
        placeholderId: CreativePlaceholderId.ProductTileDesktopLarge,
        trackingName: 'productTileDesktopLarge',
        url: 'https://www.jochen-schweizer.de',
        image: {
            url: 'https://dummyimage.com/258x362/007aa5?text=Banner-Desktop-Large',
            alt: 'Banner-Desktop-Large',
        },
        countdown: {
            color: '#fff',
            endDateUtc: '2023-02-25T13:55:00.000Z',
        },
    },
    {
        campaignName: 'Black Week',
        placeholderId: CreativePlaceholderId.ProductTileDesktopSmall,
        trackingName: 'productTileDesktopSmall',
        url: 'https://www.jochen-schweizer.de',
        image: {
            url: 'https://dummyimage.com/200x325/007aa5?text=Banner-Desktop-Medium',
            alt: 'Banner-Desktop-Medium',
        },
    },
];

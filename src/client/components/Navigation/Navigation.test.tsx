import { renderWithProviders } from '@jsmdg/react-fragment-scripts/testing';
import { screen } from '@testing-library/react';

import { navigationItems } from '../../../__fixtures__/navigationItems';
import { Navigation } from './Navigation.tsx';
import { campaignMock } from '../../../__fixtures__/campaignMock';
import { tenantConfigMock } from '../../../__fixtures__/tenantConfigMock';
import { CreativePlaceholderId } from '../../../shared/enums';

describe('Navigation', () => {
    it('when items are empty, then a fallback is rendered', () => {
        const { container } = renderWithProviders(<Navigation />, {
            fragmentContextValue: {
                tenantConfig: tenantConfigMock,
                navigationItems: [],
            },
        });

        const fallback = screen.getByTestId('fallback-side-navigation');

        expect(fallback).toBeInTheDocument();
        expect(container).toMatchSnapshot();
    });

    it('when breadcrumb is empty, then a the first navigation node will be rendered as fallback', () => {
        renderWithProviders(<Navigation />, {
            fragmentContextValue: {
                tenantConfig: tenantConfigMock,
                navigationItems,
            },
        });

        const experienceNode = screen.getByText('Alle-Erlebnisse');
        const giftsNode = screen.queryByText('fuer-paare');

        expect(experienceNode).toBeInTheDocument();
        expect(giftsNode).not.toBeInTheDocument();
    });

    it('when top breadcrumb does not match at all, then a the first navigation node will be rendered as fallback', () => {
        renderWithProviders(<Navigation />, {
            fragmentContextValue: {
                tenantConfig: tenantConfigMock,
                navigationItems,
                breadcrumb: [{ id: 'non-existing-id', url: '', title: '' }],
            },
        });

        const experienceNode = screen.getByText('Alle-Erlebnisse');
        const giftsNode = screen.queryByText('fuer-paare');

        expect(experienceNode).toBeInTheDocument();
        expect(giftsNode).not.toBeInTheDocument();
    });

    it('when top breadcrumb does not match on level 1, then the first navigation node will be rendered as fallback', () => {
        renderWithProviders(<Navigation />, {
            fragmentContextValue: {
                tenantConfig: tenantConfigMock,
                navigationItems,
                breadcrumb: [{ id: 'fallen-springen', url: '', title: '' }],
            },
        });

        const experienceNode = screen.getByText('Alle-Erlebnisse');
        const giftsNode = screen.queryByText('fuer-paare');

        expect(experienceNode).toBeInTheDocument();
        expect(giftsNode).not.toBeInTheDocument();
    });

    it('when top breadcrumb matches on level 1, then that node will be rendered', () => {
        renderWithProviders(<Navigation />, {
            fragmentContextValue: {
                tenantConfig: tenantConfigMock,
                navigationItems,
                breadcrumb: [{ id: 'fallen-springen', url: '', title: '' }],
            },
        });

        const experienceNode = screen.queryByText('Alle-Erlebnisse');

        expect(experienceNode).toBeInTheDocument();
    });

    describe('ACM navigation  banner', () => {
        it('should render banner', () => {
            renderWithProviders(<Navigation />, {
                fragmentContextValue: {
                    tenantConfig: tenantConfigMock,
                    navigationItems,
                    breadcrumb: [{ id: 'geschenke-mitarbeiter', url: '', title: '' }],
                    isHome: false,
                    campaign: campaignMock,
                },
            });
            const banner = screen.getByRole('presentation');
            expect(banner).toBeInTheDocument();
            expect(banner).toHaveAttribute('alt', 'Navigation-Banner-Desktop');
            expect(banner).toHaveAttribute('decoding', 'async');
        });

        it('should not render for Home page', () => {
            renderWithProviders(<Navigation />, {
                fragmentContextValue: {
                    tenantConfig: tenantConfigMock,
                    navigationItems,
                    breadcrumb: [{ id: 'geschenke-mitarbeiter', url: '', title: '' }],
                    isHome: true,
                    campaign: campaignMock,
                },
            });
            const banner = screen.queryByRole('presentationa');
            expect(banner).not.toBeInTheDocument();
        });

        it('should not render when campaign do not include navigation banner creative', () => {
            renderWithProviders(<Navigation />, {
                fragmentContextValue: {
                    tenantConfig: tenantConfigMock,
                    navigationItems,
                    breadcrumb: [{ id: 'geschenke-mitarbeiter', url: '', title: '' }],
                    isHome: false,
                    campaign: [
                        {
                            campaignName: 'Black Week',
                            placeholderId: CreativePlaceholderId.ProductTileDesktopSmall,
                            trackingName: 'productTileDesktopSmall',
                            url: 'https://www.jochen-schweizer.de',
                            image: {
                                url: 'https://dummyimage.com/200x325/007aa5?text=Banner-Desktop-Medium',
                                alt: 'Banner-Desktop-Medium',
                            },
                        },
                    ],
                },
            });
            const banner = screen.queryByRole('presentationa');
            expect(banner).not.toBeInTheDocument();
        });
    });
});

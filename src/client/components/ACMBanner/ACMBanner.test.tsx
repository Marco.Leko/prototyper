import { renderWithProviders } from '@jsmdg/react-fragment-scripts/testing';
import { screen } from '@testing-library/react';
import { trackEecPromotionClick, trackEecPromotionImpression } from '@jsmdg/tracking';
import userEvent from '@testing-library/user-event';

import { ACMBanner } from './ACMBanner.tsx';
import { CreativePlaceholderId } from '../../../shared/enums';

jest.mock('@jsmdg/tracking', () => ({
    trackEecPromotionImpression: jest.fn(),
    trackEecPromotionClick: jest.fn(),
}));

jest.mock('@jsmdg/yoshi/dist/hooks/useIntersection', () => ({
    useIntersection: jest.fn().mockReturnValue([true, jest.fn()]),
}));

describe('ACMBanner', () => {
    it('should render banner', () => {
        renderWithProviders(
            <ACMBanner
                image={{
                    url: 'some-image-url',
                    alt: 'some-image-alt',
                }}
                url="some-url"
                trackingName="some-trackingName"
                campaignName="some-campaignName"
                placeholderId={CreativePlaceholderId.NavigationBannerDesktop}
                countdown={{ color: '#ff0000', endDateUtc: '2023-11-13T23:00:00.000Z' }}
            />,
        );

        const banner = screen.getByRole('presentation');
        expect(banner).toBeInTheDocument();
        expect(banner).toHaveAttribute('alt', 'some-image-alt');
        expect(banner).toHaveAttribute('decoding', 'async');
    });

    it('should not render timer without endDateUtc', () => {
        renderWithProviders(
            <ACMBanner
                image={{
                    url: 'some-image-url',
                    alt: 'some-image-alt',
                }}
                url="some-url"
                trackingName="some-trackingName"
                campaignName="some-campaignName"
                placeholderId={CreativePlaceholderId.NavigationBannerDesktop}
            />,
        );
        expect(screen.queryByText('Tage')).not.toBeInTheDocument();
    });

    it('tracks promotion impression', () => {
        renderWithProviders(
            <ACMBanner
                image={{
                    url: 'some-image-url',
                    alt: 'some-image-alt',
                }}
                trackingName="some-trackingName"
                campaignName="some-campaignName"
                placeholderId={CreativePlaceholderId.NavigationBannerDesktop}
            />,
        );
        expect(trackEecPromotionImpression).toHaveBeenCalledTimes(1);
        expect(trackEecPromotionImpression).toHaveBeenCalledWith(
            [
                {
                    id: 'some-trackingName',
                    name: 'some-campaignName',
                    creative: `${CreativePlaceholderId.NavigationBannerDesktop}_CategoryLanding`,
                    position: 'vertical1_horizontal1',
                },
            ],
            {
                creativeName: 'Navigation_Banner',
                creativeSlot: 'vertical1_horizontal1',
                promotionName: 'some-campaignName',
                promotionId: 'some-trackingName',
                locationId: '',
                promotionCountdown: 'false',
                promotionPlaceholder: CreativePlaceholderId.NavigationBannerDesktop,
                promotionType: 'dynamic',
            },
        );
    });

    it('tracks promotion click', () => {
        renderWithProviders(
            <ACMBanner
                image={{
                    url: 'some-image-url',
                    alt: 'some-image-alt',
                }}
                url="some-url"
                trackingName="some-trackingName"
                campaignName="some-campaignName"
                placeholderId={CreativePlaceholderId.NavigationBannerDesktop}
            />,
        );

        userEvent.click(screen.getByRole('presentation'));

        expect(trackEecPromotionClick).toHaveBeenCalledTimes(1);
        expect(trackEecPromotionClick).toHaveBeenCalledWith(
            {
                id: 'some-trackingName',
                name: 'some-campaignName',
                creative: `${CreativePlaceholderId.NavigationBannerDesktop}_CategoryLanding`,
                position: 'vertical1_horizontal1',
            },
            {
                creativeName: 'Navigation_Banner',
                creativeSlot: 'vertical1_horizontal1',
                promotionName: 'some-campaignName',
                promotionId: 'some-trackingName',
                locationId: 'some-url',
                promotionCountdown: 'false',
                promotionPlaceholder: CreativePlaceholderId.NavigationBannerDesktop,
                promotionType: 'dynamic',
            },
        );
    });
});

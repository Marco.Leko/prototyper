import { renderWithProviders } from '@jsmdg/react-fragment-scripts/testing';

import { navigationItems } from '../../../__fixtures__/navigationItems';
import { NavigationGroup } from './NavigationGroup.tsx';
import { tenantConfigMock } from '../../../__fixtures__/tenantConfigMock';

describe('NavigationGroup', () => {
    const providerOptions = {
        fragmentContextValue: { tenantConfig: tenantConfigMock },
    };

    it('when items are empty, then it renders an empty list', () => {
        const { container } = renderWithProviders(
            <NavigationGroup
                items={[]}
                itemLevel={1}
                navigationLevel={2}
                selectedItemHasChildren
            />,
            providerOptions,
        );

        expect(container).toMatchSnapshot();
    });

    it('when items are given, then they are rendered in a list', () => {
        const { container } = renderWithProviders(
            <NavigationGroup
                items={navigationItems[0].children}
                breadcrumb={[]}
                itemLevel={1}
                navigationLevel={2}
                selectedItemHasChildren
            />,
            providerOptions,
        );

        expect(container.firstChild).toHaveAttribute('data-level', '1');
        expect(container.firstChild?.childNodes).toHaveLength(12);
    });
});

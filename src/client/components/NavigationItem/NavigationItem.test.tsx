import { renderWithProviders } from '@jsmdg/react-fragment-scripts/testing';
import { fireEvent } from '@testing-library/react';
import { trackAnalyticsEvent } from '@jsmdg/tracking/dist';

import { navigationItems } from '../../../__fixtures__/navigationItems';
import { NavigationItem } from './NavigationItem.tsx';
import { tenantConfigMock } from '../../../__fixtures__/tenantConfigMock';

jest.mock('@jsmdg/tracking');

describe('NavigationItem', () => {
    const providerOptions = {
        fragmentContextValue: {
            tenantConfig: tenantConfigMock,
        },
    };

    it('when item with id glossar is given, then it does not render anything', () => {
        const navigationItem = {
            id: 'glossar',
            title: 'Glossar',
            uri: '/erlebnisstandorte/glossar,default,pg.html',
            children: [],
        };

        const { container } = renderWithProviders(
            <NavigationItem
                item={navigationItem}
                itemLevel={1}
                navigationLevel={1}
                selectedItemHasChildren
            />,
            providerOptions,
        );

        expect(container).toMatchSnapshot();
    });

    it('when empty breadcrumb is given, then it renders an item with no highlight and no children', () => {
        const { container } = renderWithProviders(
            <NavigationItem
                item={navigationItems[0].children[7]}
                itemLevel={1}
                navigationLevel={1}
                selectedItemHasChildren
            />,
            providerOptions,
        );

        expect(container).toMatchSnapshot();
    });

    it('when a non matching breadcrumb is given, then it renders an item with no highlight and no children', () => {
        const breadcrumb = [{ id: 'a-non-matching-id', url: '', title: '' }];

        const { container } = renderWithProviders(
            <NavigationItem
                item={navigationItems[0].children[7]}
                breadcrumb={breadcrumb}
                itemLevel={1}
                navigationLevel={1}
                selectedItemHasChildren
            />,
            providerOptions,
        );

        expect(container).toMatchSnapshot();
    });

    it('when a breadcrumb with missing parent is given, then it renders an item with no highlight and no children', () => {
        // This means that the hierarchy has to match. If the root crumb does not match it will not go deeper
        const breadcrumb = [{ id: '8gwjd', url: '', title: '' }];

        const { container } = renderWithProviders(
            <NavigationItem
                item={navigationItems[0].children[7]}
                breadcrumb={breadcrumb}
                itemLevel={1}
                navigationLevel={1}
                selectedItemHasChildren
            />,
            providerOptions,
        );

        expect(container).toMatchSnapshot();
    });

    it('when breadcrumb matches on level 1, then it renders a highlighted item and its immediate children are visible', () => {
        const breadcrumb = [{ id: '26n8g', url: '', title: '' }];

        const { container } = renderWithProviders(
            <NavigationItem
                item={navigationItems[0].children[7]}
                breadcrumb={breadcrumb}
                itemLevel={1}
                navigationLevel={1}
                selectedItemHasChildren
            />,
            providerOptions,
        );

        expect(container).toMatchSnapshot();
    });

    it('when breadcrumb directly matches on level 2, then the level 2 item is highlighted and only 2 levels are visible', () => {
        const breadcrumb = [
            { id: '26n8g', url: '', title: '' },
            { id: '5o7y3', url: '', title: '' },
        ];

        const { container } = renderWithProviders(
            <NavigationItem
                item={navigationItems[0].children[7]}
                breadcrumb={breadcrumb}
                itemLevel={2}
                navigationLevel={2}
                selectedItemHasChildren
            />,
            providerOptions,
        );

        expect(container).toMatchSnapshot();
    });

    it('when breadcrumb matches on level 2 on a parent crumb, then the level 2 item is highlighted and only 2 levels are visible', () => {
        const breadcrumb = [
            { id: '26n8g', url: '', title: '' },
            { id: '5o7y3', url: '', title: '' },
            {
                id: 'jzw57',
                url: '',
                title: '',
            },
        ];

        const { container } = renderWithProviders(
            <NavigationItem
                item={navigationItems[0].children[7]}
                breadcrumb={breadcrumb}
                itemLevel={2}
                navigationLevel={2}
                selectedItemHasChildren
            />,
            providerOptions,
        );

        expect(container).toMatchSnapshot();
    });

    it('when breadcrumb matches on level 2 on a grand parent crumb, then the level 2 item is highlighted and only 2 levels are visible', () => {
        const breadcrumb = [
            { id: '26n8g', url: '', title: '' },
            { id: '5o7y3', url: '', title: '' },
            { id: 'jzw57', url: '', title: '' },
            { id: 'id_of_seo_page_not_in_test_data', url: '', title: '' },
        ];

        const { container } = renderWithProviders(
            <NavigationItem
                item={navigationItems[0].children[7]}
                breadcrumb={breadcrumb}
                itemLevel={2}
                navigationLevel={2}
                selectedItemHasChildren
            />,
            providerOptions,
        );

        expect(container).toMatchSnapshot();
    });

    it('when breadcrumb directly matches on level 3, then the level 2 item is highlighted and only 3 levels are visible', () => {
        const breadcrumb = [
            { id: '26n8g', url: '', title: '' },
            { id: '5o7y3', url: '', title: '' },
        ];

        const { container } = renderWithProviders(
            <NavigationItem
                item={navigationItems[0].children[7]}
                breadcrumb={breadcrumb}
                itemLevel={3}
                navigationLevel={3}
                selectedItemHasChildren
            />,
            providerOptions,
        );

        expect(container).toMatchSnapshot();
    });

    it.each([
        {
            urlQueryParams: '',
            expectedLocationHref: '/kunst-kultur-lifestyle/l/26n8g',
        },
        {
            urlQueryParams: '?randomParam=randomValue',
            expectedLocationHref: '/kunst-kultur-lifestyle/l/26n8g',
        },
        {
            urlQueryParams: '?lat=52.52000659999999&long=13.404954',
            expectedLocationHref: '/kunst-kultur-lifestyle/l/26n8g',
        },
        {
            urlQueryParams:
                '?lat=52.52000659999999&long=13.404954&locationName=Berlin%2C+Deutschland&distance=100',
            expectedLocationHref:
                '/kunst-kultur-lifestyle/l/26n8g?lat=52.52000659999999&long=13.404954&locationName=Berlin%2C+Deutschland&distance=100',
        },
    ])(
        'should redirect to correct url and call trackAnalyticsEvent when it is clicked',
        ({ urlQueryParams, expectedLocationHref }) => {
            const breadcrumb = [{ id: '26n8g', url: '', title: '' }];
            const originalWindowLocation = window.location;

            Object.defineProperty(window, 'location', {
                value: {
                    search: urlQueryParams,
                },
                writable: true,
            });

            const { getByRole } = renderWithProviders(
                <NavigationItem
                    item={navigationItems[0].children[7]}
                    breadcrumb={breadcrumb}
                    itemLevel={1}
                    navigationLevel={2}
                    selectedItemHasChildren
                    trackingLabel="Test label"
                />,
                providerOptions,
            );
            const navLink = getByRole('link', { name: 'Kunst, Kultur & Lifestyle' });

            fireEvent.click(navLink);

            expect(trackAnalyticsEvent).toHaveBeenCalledWith({
                category: 'SideNavi',
                action: 'Click',
                label: 'Test label',
            });
            expect(window.location.href).toEqual(expectedLocationHref);

            window.location = originalWindowLocation;
        },
    );
});

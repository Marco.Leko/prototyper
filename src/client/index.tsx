import { createFragment } from '@jsmdg/react-fragment-scripts/fragment';

import { Navigation } from './components/Navigation/Navigation.tsx';
import { RootComponentProps } from '../shared/types';
import { FragmentContext } from './types/fragmentContext.ts';

const { Fragment, init } = createFragment<RootComponentProps, FragmentContext>(
    Navigation,
    ({ rootComponentProps }) => ({ ...rootComponentProps }),
);

export { Fragment, init };

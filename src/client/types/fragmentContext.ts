import { BreadCrumbItem, Creative, NavigationItem } from '../../shared/types';
import { TenantConfig } from '../../shared/types/tenantConfig';

export type FragmentContext = {
    tenantConfig: TenantConfig;
    navigationItems: NavigationItem[];
    breadcrumb?: BreadCrumbItem[];
    isHome?: boolean;
    campaign?: Creative[];
    isMydays: boolean;
};

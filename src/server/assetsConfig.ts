import {
    ProtocolVersion,
    Loading,
    Position,
    Priority,
    Cors,
    LinkType,
} from '@jsmdg/fragment-protocol';
import { FragmentProtocolObject } from '@jsmdg/react-fragment-scripts';
import { AsyncLocalStorage } from 'async_hooks';

const assetsStaticConfig: FragmentProtocolObject = {
    //! even if empty, mandatory as empty array
    //! applies to script, styles, preload, prefetch.
    scripts: [
        {
            url: 'main.js',
            loading: Loading.LAZY,
            position: Position.END,
            priority: Priority.OMIT,
            cors: Cors.OMIT,
        },
    ],
    styles: [
        {
            url: 'main.css',
            loading: Loading.BLOCKING,
            position: Position.INLINE,
            priority: Priority.OMIT,
            cors: Cors.OMIT,
            media: '(min-width: 992px)',
        },
    ],
    preload: [
        {
            url: 'main.css',
            as: LinkType.Style,
            priority: Priority.OMIT,
            cors: Cors.OMIT,
            media: '(min-width: 992px)',
            type: '',
        },
    ],
    prefetch: [],
    v: ProtocolVersion.V2,
};

//! This is the object that should be written during runtime
//! and gets reset every request
const assetsDynamicConfig = new AsyncLocalStorage<FragmentProtocolObject>();

export { assetsStaticConfig, assetsDynamicConfig };

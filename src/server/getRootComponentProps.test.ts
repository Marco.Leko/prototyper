import { mockLogger } from '@jsmdg/logger';

import { getRootComponentProps } from './getRootComponentProps';
import { fetchCampaign } from './services/fetchCampaign';
import { fetchBreadcrumb } from './services/fetchBreadcrumb';
import { fetchNavigationStructure } from './services/fetchNavigationStructure';
import { RootComponentProps } from '../shared/types';
import { CreativePlaceholderId } from '../shared/enums';

jest.mock('./services/fetchNavigationStructure', () => ({
    fetchNavigationStructure: jest.fn(),
}));

jest.mock('./services/fetchBreadcrumb', () => ({
    fetchBreadcrumb: jest.fn(),
}));

jest.mock('./services/fetchCampaign', () => ({
    fetchCampaign: jest.fn(),
}));

const { env } = process;

beforeEach(() => {
    process.env = {
        ...env,
    };
});

afterEach(() => {
    process.env = env;
});

describe('getRootComponentProps', () => {
    const payload = {
        query: {
            type: 'type',
            objectId: 'objectId',
            isHome: 'true',
        },
        tenant: 'JS_DE',
        locale: 'de-DE',
        logger: mockLogger(),
        featureFlagsVariation: {},
    };

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('call fetchNavigationStructure', async () => {
        await getRootComponentProps(payload);

        expect(fetchNavigationStructure).toHaveBeenCalledTimes(1);
    });

    it('call fetchBreadcrumb', async () => {
        await getRootComponentProps(payload);

        expect(fetchBreadcrumb).toHaveBeenCalledTimes(1);
    });

    it('call fetchCampaign', async () => {
        await getRootComponentProps(payload);

        expect(fetchCampaign).toHaveBeenCalledTimes(1);
    });

    it('use NAVIGATION_HOST as the navigationBackendBaseUrl', async () => {
        process.env = {
            ...env,
            NAVIGATION_HOST: 'foo',
        };
        await getRootComponentProps(payload);

        expect(fetchNavigationStructure).toHaveBeenCalledWith(
            expect.objectContaining({
                baseUrl: 'foo',
            }),
        );
        expect(fetchBreadcrumb).toHaveBeenCalledWith(
            expect.objectContaining({
                baseUrl: 'foo',
            }),
        );
    });

    it('correctly cast isHome query property', async () => {
        const rootComponentProps = await getRootComponentProps({
            ...payload,
            query: {
                isHome: 'true',
            },
        });

        expect(rootComponentProps).toEqual(
            expect.objectContaining({
                isHome: true,
            }),
        );
    });

    it('correctly map fetched data into return', async () => {
        const navigationStructureMock = [
            {
                id: 'navigationItemId',
                title: 'navigationItemTitle',
                uri: 'navigationItemUri',
                children: [],
            },
        ];
        const breadcrumbsMock = [
            { id: 'breadcrumbId', title: 'breadcrumbTitle', url: 'breadcrumbUrl' },
        ];
        const campaignMock = [
            {
                campaignName: 'campaignName',
                placeholderId: CreativePlaceholderId.NavigationBannerDesktop,
                trackingName: 'campaignTrackingName',
                image: {
                    url: 'campaignImageUrl',
                    alt: 'campaignImageAlt',
                },
            },
        ];

        jest.mocked(fetchNavigationStructure).mockResolvedValue(navigationStructureMock);
        jest.mocked(fetchBreadcrumb).mockResolvedValue(breadcrumbsMock);
        jest.mocked(fetchCampaign).mockResolvedValue(campaignMock);

        const rootComponentProps = await getRootComponentProps({
            ...payload,
            query: {
                isHome: 'false',
            },
            featureFlagsVariation: { foo: 'bar' },
        });

        expect(rootComponentProps).toEqual<RootComponentProps>({
            navigationItems: navigationStructureMock,
            breadcrumb: breadcrumbsMock,
            isHome: false,
            isMydays: false,
            featureFlagsVariation: { foo: 'bar' },
            campaign: campaignMock,
            tenantConfig: {
                allExperiencesPageId: '79ifa',
                giftBoxPages: [
                    '/geschenkbox/',
                    '/fun-action-erlebnisboxen/',
                    '/kurzurlaub-erlebnisboxen/',
                    '/wellness-lifestyle-erlebnisboxen/',
                    '/zeit-zu-zweit-erlebnisboxen/',
                ],
                glossarPageId: 'glossar',
            },
            showBetaFeatures: false,
        });
    });
});

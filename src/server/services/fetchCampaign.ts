import axios, { AxiosResponse } from 'axios';
import { LoggerInterface } from '@jsmdg/logger';

import { Creative } from '../../shared/types';

type FetchCampaignParams = {
    tenant: string;
    locale: string;
    logger: LoggerInterface;
};

const fetchCampaign = async ({
    tenant,
    locale,
    logger,
}: FetchCampaignParams): Promise<Creative[]> => {
    const host = process.env.CAMPAIGN_HOST || 'http://campaign-provider';

    try {
        const { data }: AxiosResponse<{ creatives: Creative[] }> = await axios.get(
            `${host}/api/v1/campaigns/creatives/`,
        );

        if (data?.creatives) {
            return data.creatives;
        }

        return [];
    } catch (error) {
        logger
            .withException(error as Error)
            .withFields({ tenant, locale })
            .warning('failed to fetch campaign from campaign-provider');

        return [];
    }
};

export { fetchCampaign };

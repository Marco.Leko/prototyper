import mockAxios from 'axios';
import { LoggerInterface, LogLevel } from '@jsmdg/logger';

import { fetchCampaign } from './fetchCampaign';

import Mock = jest.Mock;

const { env } = process;

beforeEach(() => {
    process.env = {
        ...env,
    };
});

afterEach(() => {
    process.env = env;
});

describe('fetchCampaign', () => {
    const payload = {
        tenant: 'JS_DE',
        locale: 'de-DE',
        logger: {} as LoggerInterface,
    };

    it('should make a network request', async () => {
        (<Mock>mockAxios.get).mockImplementationOnce(() =>
            Promise.resolve({
                data: 'some string',
            }),
        );

        await fetchCampaign(payload);

        expect(<Mock>mockAxios.get).toHaveBeenCalledTimes(1);
        expect(<Mock>mockAxios.get).toHaveBeenCalledWith(
            'http://campaign-provider/api/v1/campaigns/creatives/',
        );
    });

    it('returns response data', async () => {
        (<Mock>mockAxios.get).mockImplementationOnce(() =>
            Promise.resolve({
                data: { creatives: 'mock string' },
            }),
        );

        const result = await fetchCampaign(payload);

        expect(result).toBe('mock string');
    });

    it('returns empty array as default value if there is no campaign', async () => {
        (<Mock>mockAxios.get).mockImplementationOnce(() =>
            Promise.resolve({
                data: null,
            }),
        );

        const result = await fetchCampaign(payload);

        expect(result).toEqual([]);
    });
    it('returns empty array and logs an error if something fails', async () => {
        const error = new Error('something failed');
        (<Mock>mockAxios.get).mockImplementationOnce(() => Promise.reject(error));

        const mockLogger = {
            withException: jest.fn().mockReturnThis(),
            withFields: jest.fn().mockReturnThis(),
            warning: jest.fn(),
            debug: jest.fn(),
            error: jest.fn(),
            info: jest.fn(),
            withField: jest.fn().mockReturnThis(),
            child: jest.fn().mockReturnThis(),
            trace: jest.fn(),
            warn: jest.fn(),
            fatal: jest.fn(),
            silent: jest.fn(),
            level: LogLevel.INFO,
        };
        const result = await fetchCampaign({ ...payload, logger: mockLogger });

        expect(result).toEqual([]);
        expect(mockLogger.withException).toHaveBeenCalledTimes(1);
        expect(mockLogger.withException).toHaveBeenCalledWith(error);
        expect(mockLogger.withFields).toHaveBeenCalledTimes(1);
        expect(mockLogger.withFields).toHaveBeenCalledWith({
            tenant: 'JS_DE',
            locale: 'de-DE',
        });
        expect(mockLogger.warning).toHaveBeenCalledTimes(1);
        expect(mockLogger.warning).toHaveBeenCalledWith(
            'failed to fetch campaign from campaign-provider',
        );
    });
});

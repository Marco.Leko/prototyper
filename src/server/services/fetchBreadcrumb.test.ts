import mockAxios from 'axios';
import Logger from '@jsmdg/logger';

import { fetchBreadcrumb } from './fetchBreadcrumb';

import Mock = jest.Mock;

const { env } = process;

beforeEach(() => {
    process.env = {
        ...env,
    };
});

afterEach(() => {
    process.env = env;
});

describe('fetchBreadcrumb', () => {
    describe('fetchBreadcrumb', () => {
        it('if objectId is empty then an empty array is returned and no network request is made', async () => {
            const result = await fetchBreadcrumb({
                baseUrl: '',
                tenant: 'JS_DE',
                locale: 'de-DE',
                type: 'test-type',
                logger: {} as Logger,
                objectId: '',
            });

            expect(result).toBeDefined();
            expect(result).toHaveLength(0);
            expect(<Mock>mockAxios.get).toHaveBeenCalledTimes(0);
        });

        it('if type is empty then an empty array is returned and no network request is made', async () => {
            const result = await fetchBreadcrumb({
                baseUrl: '',
                tenant: 'JS_DE',
                locale: 'de-DE',
                type: '',
                logger: {} as Logger,
                objectId: 'id',
            });

            expect(result).toBeDefined();
            expect(result).toHaveLength(0);
            expect(<Mock>mockAxios.get).toHaveBeenCalledTimes(0);
        });

        it('should make a network request', async () => {
            (<Mock>mockAxios.get).mockImplementationOnce(() =>
                Promise.resolve({
                    data: {
                        test: 'does not matter',
                    },
                }),
            );

            const payload = {
                baseUrl: 'http://example.test',
                tenant: 'JS_DE',
                locale: 'de-DE',
                type: 'test',
                objectId: 'testId',
                logger: {} as Logger,
            };

            await fetchBreadcrumb(payload);

            expect(<Mock>mockAxios.get).toHaveBeenCalledTimes(1);
            expect(<Mock>mockAxios.get).toHaveBeenCalledWith(
                'http://example.test/api/v1/test/testId/breadcrumbs',
                {
                    headers: {
                        'Accept-Language': payload.locale,
                        'X-Tenant': payload.tenant,
                    },
                },
            );
        });

        it('returns an empty array and logs an error if something fails', async () => {
            const expectedError = new Error('something failed');
            const mockLogger = {
                withException: jest.fn().mockReturnThis(),
                withFields: jest.fn().mockReturnThis(),
                warning: jest.fn(),
            } as Partial<Logger>;
            (<Mock>mockAxios.get).mockImplementationOnce(() => Promise.reject(expectedError));

            const payload = {
                baseUrl: 'http://example.test',
                tenant: 'JS_DE',
                locale: 'de-DE',
                type: 'test',
                objectId: 'testId',
                logger: mockLogger as Logger,
            };

            const breadcrumb = await fetchBreadcrumb(payload);

            expect(breadcrumb).not.toBeNull();
            expect(breadcrumb).toHaveLength(0);
            expect(mockLogger.withException).toHaveBeenCalledWith(expectedError);
            expect(mockLogger.warning).toHaveBeenCalled();
        });
    });
});

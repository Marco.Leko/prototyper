import axios, { AxiosResponse } from 'axios';
import { LoggerInterface } from '@jsmdg/logger';

import { NavigationItem } from '../../shared/types';

type FetchNavigationStructureParams = {
    baseUrl: string;
    tenant: string;
    locale: string;
    logger: LoggerInterface;
};

async function fetchNavigationStructure({
    tenant,
    locale,
    baseUrl,
    logger,
}: FetchNavigationStructureParams): Promise<Array<NavigationItem>> {
    const path = '/api/v2/navigation/default';

    try {
        const result: AxiosResponse<Array<NavigationItem>> = await axios.get(baseUrl + path, {
            headers: {
                'Accept-Language': locale,
                'X-Tenant': tenant,
            },
        });

        return result.data;
    } catch (err) {
        logger
            .withException(err as Error)
            .withFields({ locale, tenant })
            .warning('failed to fetch navigation structure');

        return [];
    }
}

export { fetchNavigationStructure };

import axios, { AxiosResponse } from 'axios';
import { LoggerInterface } from '@jsmdg/logger';

import { BreadCrumbItem, Breadcrumbs } from '../../shared/types';

type FetchBreadcrumbParams = {
    baseUrl: string;
    tenant: string;
    locale: string;
    type: string;
    objectId: string;
    logger: LoggerInterface;
};

/**
 * Fetches breadcrumb for given object. Defaults to self if no breadcrumb is found
 */
async function fetchBreadcrumb({
    baseUrl,
    tenant,
    locale,
    type,
    objectId,
    logger,
}: FetchBreadcrumbParams): Promise<Array<BreadCrumbItem>> {
    if (!objectId || !type) {
        // this data is not always provided, e.g. search
        return [];
    }

    try {
        const response: AxiosResponse<Breadcrumbs> = await axios.get(
            `${baseUrl}/api/v1/${type}/${objectId}/breadcrumbs`,
            {
                headers: {
                    'Accept-Language': locale,
                    'X-Tenant': tenant,
                },
            },
        );

        return response.data.breadcrumbs;
    } catch (error) {
        logger
            .withFields({
                type,
                objectId,
                tenant,
                locale,
            })
            .withException(error as Error)
            .warning('failed to fetch breadcrumb from navigation-backend');

        return [];
    }
}

export { fetchBreadcrumb };

import { createServer } from '@jsmdg/react-fragment-scripts';

import { getRootComponentProps } from './getRootComponentProps';
import { assetsStaticConfig, assetsDynamicConfig } from './assetsConfig';

// the createServer function from react-fragment-scripts initialises an fastify server
// and registers the / route which returns the fragment's initial html and relevant assets
// eslint-disable-next-line @typescript-eslint/no-floating-promises
createServer({
    getRootComponentProps,
    assetsStaticConfig,
    assetsDynamicConfig,
});

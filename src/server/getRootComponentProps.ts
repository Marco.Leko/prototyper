import { RootComponentPropsParams } from '@jsmdg/react-fragment-scripts';
import { TenantEnum, Brand, Tenant } from '@jsmdg/tenancy';

import { RootComponentProps } from '../shared/types';
import { fetchNavigationStructure } from './services/fetchNavigationStructure';
import { fetchBreadcrumb } from './services/fetchBreadcrumb';
import { fetchCampaign } from './services/fetchCampaign';
import { getTenantConfig } from '../shared/helpers/getTenantConfig';

// a fragment can have initial data which can be fetched asynchronously
// the params passed to this function are the query parameters
// eslint-disable-next-line max-params
export async function getRootComponentProps({
    query,
    tenant,
    locale,
    logger,
    featureFlagsVariation = {},
}: RootComponentPropsParams): Promise<RootComponentProps> {
    const { showBetaFeatures, type, objectId, isHome } = query;
    const navigationBackendBaseUrl = process.env.NAVIGATION_HOST || 'http://navigation-backend';

    const [navigationItems, breadcrumb, creatives] = await Promise.all([
        fetchNavigationStructure({
            tenant,
            locale,
            baseUrl: navigationBackendBaseUrl,
            logger,
        }),
        fetchBreadcrumb({
            tenant,
            locale,
            type: type as string,
            objectId: objectId as string,
            baseUrl: navigationBackendBaseUrl,
            logger,
        }),
        fetchCampaign({
            tenant,
            locale,
            logger,
        }),
    ]);

    return {
        showBetaFeatures: showBetaFeatures === 'true',
        navigationItems,
        breadcrumb,
        isHome: isHome === 'true',
        featureFlagsVariation,
        campaign: creatives,
        tenantConfig: getTenantConfig(tenant as TenantEnum),
        isMydays: new Tenant(tenant as TenantEnum).belongsTo(Brand.Mydays),
    };
}

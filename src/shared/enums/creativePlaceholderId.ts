export enum CreativePlaceholderId {
    ProductTileDesktopLarge = 'product-tile-desktop-large',
    ProductTileDesktopSmall = 'product-tile-desktop-small',
    ProductTileTablet = 'product-tile-tablet',
    ProductTileMobile = 'product-tile-mobile',
    NavigationBannerDesktop = 'navigation-banner-desktop',
    SelectionBlockDesktop = 'selection-block-desktop',
}

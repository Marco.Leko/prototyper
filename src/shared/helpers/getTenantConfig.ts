import { TenantEnum } from '@jsmdg/tenancy';

import { TenantConfig } from '../types';

const jsDeTenantConfig: TenantConfig = {
    giftBoxPages: [
        '/geschenkbox/',
        '/fun-action-erlebnisboxen/',
        '/kurzurlaub-erlebnisboxen/',
        '/wellness-lifestyle-erlebnisboxen/',
        '/zeit-zu-zweit-erlebnisboxen/',
    ],
    glossarPageId: 'glossar',
    allExperiencesPageId: '79ifa',
};

/**
 * TODO: Update mydays config
 */
const tenantConfigMapping: Record<string, TenantConfig> = {
    [TenantEnum.JS_DE]: jsDeTenantConfig,
    [TenantEnum.JS_AT]: jsDeTenantConfig,
    [TenantEnum.MD_DE]: jsDeTenantConfig,
    [TenantEnum.MD_AT]: jsDeTenantConfig,
    [TenantEnum.MD_CH]: jsDeTenantConfig,
};

const getTenantConfig = (tenant: TenantEnum): TenantConfig => {
    return tenantConfigMapping[tenant] || tenantConfigMapping[TenantEnum.JS_DE];
};

export { getTenantConfig };

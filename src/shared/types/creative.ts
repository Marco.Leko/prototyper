import { CreativePlaceholderId } from '../enums/creativePlaceholderId';

export type Creative = {
    campaignName: string;
    placeholderId: CreativePlaceholderId;
    trackingName: string;
    image: {
        url: string;
        alt: string;
    };
    url?: string;
    countdown?: {
        color: string;
        endDateUtc: string;
    };
};

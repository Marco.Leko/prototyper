export interface NavigationItem {
    id: string;
    title: string;
    uri: string;
    children: NavigationItem[];
}

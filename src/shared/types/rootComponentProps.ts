import { BreadCrumbItem } from './breadcrumbs';
import { Creative } from './creative';
import { NavigationItem } from './navigationItem';
import { TenantConfig } from './tenantConfig';

type FeatureFlagsVariation = Record<string, string>;

export interface RootComponentProps {
    navigationItems: Array<NavigationItem>;
    breadcrumb: Array<BreadCrumbItem>;
    isHome: boolean;
    featureFlagsVariation: FeatureFlagsVariation;
    campaign: Creative[];
    tenantConfig: TenantConfig;
    showBetaFeatures: boolean;
    isMydays: boolean;
}

export interface BreadCrumbItem {
    id: string;
    title: string;
    url: string;
}

export interface Breadcrumbs {
    breadcrumbs: BreadCrumbItem[];
}

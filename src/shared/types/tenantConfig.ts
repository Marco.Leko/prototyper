export type TenantConfig = {
    giftBoxPages: string[];
    glossarPageId: string;
    allExperiencesPageId: string;
};

## TODO's to integrate vite

This will be a step-to-step walkthrough with different levels and increased difficulties.

1. Setup bundle & build command for fragment (Marco)
- 1. with index.tsx entry point
- 2. with css/scss file modules
- 3. with SVG's and images (check this out)
- 4. with external components like yoshi
- 5. with sentry plugin for vite
- 6. with client environment variables
- 7. with assets as inline data-uri
- 8. with different browser stages
- 9. with defining externals like react/react-dom, etc.

2. Setup local dev-server with hot-reloading (Alex)
- 1. with dev.tsx entry point
- 2. with mock data
- 3. with html generation plugin and requiring koopa in hot-reloading mode

3. Setup build for libraries (Optional)

4. Setup bundle & build command for server (Optional)
- 1. with ES modules
- 2. with option to bundle client code based on configuration